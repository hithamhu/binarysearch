/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package binarysearchdemo;


public class BinaryTree {
 TreeNode root;
 
    public BinaryTree(TreeNode root) {
        this.root=root;
       
    }
    
//    add node to tree
//    if value< from root add to left
//    if value > from root add to right
    
    public void addNode(TreeNode newNode,TreeNode rootExplore){
        //to stop added 
        if(rootExplore==null)
            return;
                
        //add to right
        if(newNode.value> rootExplore.value){
            if(rootExplore.right==null){
                rootExplore.right=newNode;
            }else{
                //if rootExplore have child in right
                //return the operation
                addNode(newNode, rootExplore.right);
            }
                   
        }
          //add to left
        if(newNode.value< rootExplore.value){
            if(rootExplore.left==null){
                rootExplore.left=newNode;
            }else{
                addNode(newNode, rootExplore.left);
            }
                   
        }
               
    }
    
    public void search(int value,TreeNode rootExplore){
        if(rootExplore==null){
            System.err.println("Value Not Found");
        return;
        }
        if(value==rootExplore.value){
            System.out.println("Value is Found: "+value);
        }
        
        if(value>rootExplore.value){
            search(value, rootExplore.right);
        }
        if(value<rootExplore.value){
            search(value, rootExplore.left);
        }
                
               
    }
    
    public void delete(int value,TreeNode rootExplore){
      root=deleteRecusive(value, rootExplore);
    }
    
    private TreeNode deleteRecusive(int value,TreeNode current){
        if (current == null) {
            return null;
        }

        if (value == current.value) {
            // Case 1: no children
            if (current.left == null && current.right == null) {
                return null;
            }

            // Case 2: only 1 child
            if (current.right == null) {
                return current.left;
            }

            if (current.left == null) {
                return current.right;
            }

            // Case 3: 2 children
            int smallestValue = findSmallestValue(current.right);
            current.value = smallestValue;
            current.right = deleteRecusive(smallestValue,current.right);
            return current;
        }
        if (value < current.value) {
            current.left = deleteRecusive(value,current.left);
            return current;
        }

        current.right = deleteRecusive( value,current.right);
        return current;
                
    }
    
    private int findSmallestValue(TreeNode root) {
    return root.left == null ? root.value : findSmallestValue(root.left);
}
    
    
    public void orderElement(TreeNode root){
        if(root !=null){
            orderElement(root.left);
            System.out.println(root.value);
            orderElement(root.right);
        }
    }
    
    
    
}
