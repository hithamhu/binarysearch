/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package binarysearchdemo;

/**
 *
 * @author Dream Tech
 */
public class TreeNode {
    int value;
    TreeNode left;
    TreeNode right;

    public TreeNode(int value,TreeNode left,TreeNode right) {
        this.value=value;
        this.left=left;
        this.right=right;
               
    }
    
}
