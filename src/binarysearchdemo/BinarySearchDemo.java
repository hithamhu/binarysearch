/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package binarysearchdemo;

/**
 *
 * @author Dream Tech
 */
public class BinarySearchDemo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        TreeNode root=new TreeNode(7, null, null);
        BinaryTree binaryTree=new BinaryTree(root);
        //add
        binaryTree.addNode(new TreeNode(5, null, null), root);
        binaryTree.addNode(new TreeNode(10, null, null), root);
        binaryTree.addNode(new TreeNode(1, null, null), root);
        binaryTree.addNode(new TreeNode(60, null, null), root);
        
        binaryTree.search(60, root);
       // binaryTree.search(6, root);
      
        binaryTree.delete(60, root);
         binaryTree.search(60, root);
         binaryTree.orderElement(root);
    }
    
}
